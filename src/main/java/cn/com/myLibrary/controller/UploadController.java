package cn.com.myLibrary.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/30  10:45
 * @Version 1.0
 * @Param $
 * @return $
 */
@Controller
@RequestMapping("/file")
public class UploadController {
    @Autowired
    private Environment env;
    // 文件存储位置
    private final static String FILESERVER="http://localhost:8080/upload/";
    @RequestMapping("/upload")
    @ResponseBody
    public Map<String, String> upload(MultipartFile headPhoto, HttpServletRequest req) throws IOException {

        //System.out.println("fileServerUrl = " + fileServerUrl);
        Map<String, String> map = new HashMap<>();
        // 获取文件名
        /*System.out.println("ajax productId:"+productId);*/
        String originalFilename = headPhoto.getOriginalFilename();
        // 避免文件名冲突,使用UUID替换文件名
        String uuid = UUID.randomUUID().toString();
        // 获取拓展名
        String extendsName = originalFilename.substring(originalFilename.lastIndexOf("."));
        // 新的文件名
        String newFileName = uuid.concat(extendsName);
        // 创建 sun公司提供的jersey包中的client对象
        Client client = Client.create();
        WebResource resource = client.resource(FILESERVER + newFileName);

        //  文件保存到另一个服务器上去了
        resource.put(String.class, headPhoto.getBytes());
        // 上传成功之后,把文件的名字和文件的类型返回给浏览器
        map.put("message", "上传成功");
        map.put("newFileName", newFileName);
        map.put("filetype", headPhoto.getContentType());
        return map;
    }
}
