package cn.com.myLibrary.controller;

import cn.com.myLibrary.pojo.EbookCateGory;
import cn.com.myLibrary.pojo.EbookEntry;
import cn.com.myLibrary.service.IEbookCateGoryService;
import cn.com.myLibrary.service.IEbookEntryService;
import cn.com.myLibrary.tools.Constants;
import cn.com.myLibrary.tools.PageSupport;
import cn.com.myLibrary.util.AjaxResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  15:24
 * @Version 1.0
 * @Param $
 * @return $
 */
@RestController
@RequestMapping("/book")
public class EbookEntryController {
    @Autowired
    private IEbookEntryService ebookEntryService;

    @Autowired
    private IEbookCateGoryService ebookCateGoryService;


    /**
     * 查询列表
     */
    @GetMapping("/list")
    public void list(EbookEntry ebookEntry, HttpServletResponse response, HttpServletRequest request) throws IOException {
        // 1、接收参数
        String queryBookName = request.getParameter("queryBookName");
        String category = request.getParameter("category");
        String pageIndex = request.getParameter("pageIndex");

        System.out.println("queryBookName = " + queryBookName);
        System.out.println("category = " + category);


        // 2、参数初始化
        EbookCateGory ebookCateGory = new EbookCateGory();
        Long categoryId = 0L;
        //设置页面容量
        int pageSize = Constants.pageSize;
        //当前页码
        int currentPageNo = 1;


        // 3、验证前端参数
        if (queryBookName == null) {
            queryBookName = "";
        } else {
            ebookEntry.setTitle(queryBookName);
        }

        if (category != null && !category.equals("") && !category.equals("0")) {
            categoryId = Long.parseLong(category);
            ebookEntry.setCategoryId(categoryId);
            ebookCateGory.setId(categoryId);
        }

        if (pageIndex != null) {
            try {
                currentPageNo = Integer.valueOf(pageIndex);
            } catch (NumberFormatException e) {
                response.sendRedirect("error.jsp");
            }
        }


        //4、 设置PageSupport
        //总数量（表）
        int totalCount = ebookEntryService.selectEbookEntryCount(ebookEntry);
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(currentPageNo);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);


        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (currentPageNo < 1) {
            currentPageNo = 1;
        } else if (currentPageNo > totalPageCount) {
            currentPageNo = totalPageCount;
        }

        // 5、查询page
        PageInfo<EbookEntry> page = ebookEntryService.selectEbookEntryList(ebookEntry, currentPageNo, pageSize);
        List<EbookCateGory> cateGoryList = ebookCateGoryService.selectEbookCateGoryList(ebookCateGory);

        request.setAttribute("ebookEntryList", page.getList());
        request.setAttribute("queryBookName", queryBookName);
        request.setAttribute("category", categoryId);
        request.setAttribute("categoryList", cateGoryList);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", currentPageNo);
        try {
            request.getRequestDispatcher("/jsp/userlist.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询所有图书分类
     */


    /**
     * 获取详细信息
     */
    @GetMapping(value = "/query")
    public void getInfo(@RequestParam("bookid") Long bookid, HttpServletResponse response, HttpServletRequest request) {
        System.out.println("执行查询操作");
        EbookEntry ebookEntry = ebookEntryService.selectEbookEntryById(bookid);

        request.setAttribute("book", ebookEntry);
        try {
            request.getRequestDispatcher("/jsp/usermodify.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 新增
     */
    @PostMapping("/save")
    public AjaxResult add(@Valid @RequestBody EbookEntry ebookEntry, BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Map<String,String> fieldMap = new HashMap<>();
        if (!fieldErrors.isEmpty()) {
            for (FieldError e :fieldErrors) {
                System.out.println(e.getCode() + "," + e.getDefaultMessage()+ "," + e.getField());
                fieldMap.put(e.getField().toString(),e.getDefaultMessage());
            }
            return AjaxResult.error(fieldErrors.get(0).getDefaultMessage(),fieldMap);
        }

        Integer addFlag = 0;
        if (isFieldsAddNotNull(ebookEntry)) {
            addFlag = ebookEntryService.insertEbookEntry(ebookEntry);
            if (addFlag >= 1) {
                return AjaxResult.success("添加成功");
            } else if (addFlag == -1) {
                return AjaxResult.success("该图书已存在");
            } else {
                return AjaxResult.error("添加失败");
            }
        }
        return AjaxResult.error();
    }

    /**
     * 修改
     */
    @PutMapping(value = "/update")
    public AjaxResult edit(@RequestBody EbookEntry ebookEntry, BindingResult bindingResult) {
        Integer updateFlag = 0;
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        if (isFieldsEditNotNull(ebookEntry) && allErrors.isEmpty()) {
            updateFlag = ebookEntryService.updateEbookEntry(ebookEntry);
            if (updateFlag >= 1) {
                return AjaxResult.success("保存成功", updateFlag);
            } else {
                return AjaxResult.error("请检查，保存失败");
            }
        } else {
            return AjaxResult.error("请检查，保存失败");
        }
    }

    /**
     * 删除
     */
    @DeleteMapping("/{ids}")
    public void remove(@PathVariable Long[] ids) {
        int i = ebookEntryService.deleteEbookEntryByIds(ids);
    }

    /**
     * 单个删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public AjaxResult remove(@RequestParam("id") Long id) {
        System.out.println("删除执行");
        System.out.println("id = " + id);
        Integer deleteFlag = 0;
        if (id != null && id != 0) {
            deleteFlag = ebookEntryService.deleteEbookEntryById(id);
            if (deleteFlag >= 1) {
                return AjaxResult.success("删除成功", deleteFlag);
            } else {
                return AjaxResult.error("请检查，删除失败");
            }
        } else {
            return AjaxResult.error("请检查，删除失败");
        }
    }


    public boolean isFieldsEditNotNull(EbookEntry ebookEntry) {
        return ebookEntry.getId() != null && ebookEntry.getCategoryId() != null && ebookEntry.getTitle() != null && ebookEntry.getCreateDate() != null;
    }

    public boolean isFieldsAddNotNull(EbookEntry ebookEntry) {
        return ebookEntry.getCategoryId() != null && ebookEntry.getTitle() != null && ebookEntry.getCreateDate() != null;
    }
}
