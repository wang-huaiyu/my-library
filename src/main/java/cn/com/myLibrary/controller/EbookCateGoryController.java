package cn.com.myLibrary.controller;

import cn.com.myLibrary.pojo.EbookCateGory;
import cn.com.myLibrary.service.IEbookCateGoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  17:41
 * @Version 1.0
 * @Param $
 * @return $
 */
@RestController
@RequestMapping("/bookCateGory")
public class EbookCateGoryController {

    @Autowired
    private IEbookCateGoryService ebookCateGoryService;

    @GetMapping("/list")
    public List<EbookCateGory> getCategoryList(EbookCateGory ebookCateGory) {
        List<EbookCateGory> cateGoryList = ebookCateGoryService.selectEbookCateGoryList(ebookCateGory);
        return cateGoryList;
    }
}
