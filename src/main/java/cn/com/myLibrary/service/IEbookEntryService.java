package cn.com.myLibrary.service;

import cn.com.myLibrary.pojo.EbookEntry;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  15:21
 * @Version 1.0
 * @Param $
 * @return $
 */
public interface IEbookEntryService {

    /**
     * 查询
     *
     * @param id 主键
     * @return
     */
    public EbookEntry selectEbookEntryById(Long id);

    /**
     * 查询列表
     *
     * @param ebookEntry
     * @return 集合
     */
    public PageInfo<EbookEntry> selectEbookEntryList(EbookEntry ebookEntry, Integer currentPageNo, Integer pageSize);

    /**
     * 新增
     *
     * @param ebookEntry
     * @return 结果
     */
    public int insertEbookEntry(EbookEntry ebookEntry);

    /**
     * 修改
     *
     * @param ebookEntry
     * @return 结果
     */
    public int updateEbookEntry(EbookEntry ebookEntry);

    /**
     * 批量删除
     *
     * @param ids 需要删除的主键集合
     * @return 结果
     */
    public int deleteEbookEntryByIds(Long[] ids);

    /**
     * 删除信息
     *
     * @param id 主键
     * @return 结果
     */
    public int deleteEbookEntryById(Long id);

    /**
     * 查询数量
     */
    public int selectEbookEntryCount(EbookEntry ebookEntry);
}

