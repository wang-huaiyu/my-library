package cn.com.myLibrary.service;

import cn.com.myLibrary.pojo.EbookCateGory;
import cn.com.myLibrary.pojo.EbookEntry;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  17:26
 * @Version 1.0
 * @Param $
 * @return $
 */
public interface IEbookCateGoryService {
    /**
     * 查询
     *
     * @param id 主键
     * @return
     */
    public EbookCateGory selectEbookCateGoryById(Long id);

    /**
     * 查询列表
     *
     * @param EbookCateGory
     * @return 集合
     */
    public List<EbookCateGory> selectEbookCateGoryList(EbookCateGory EbookCateGory);



    /**
     * 新增
     *
     * @param EbookCateGory
     * @return 结果
     */
    public int insertEbookCateGory(EbookCateGory EbookCateGory);

    /**
     * 修改
     *
     * @param EbookCateGory
     * @return 结果
     */
    public int updateEbookCateGory(EbookCateGory EbookCateGory);

    /**
     * 批量删除
     *
     * @param ids 需要删除的主键集合
     * @return 结果
     */
    public int deleteEbookCateGoryByIds(Long[] ids);

    /**
     * 删除信息
     *
     * @param id 主键
     * @return 结果
     */
    public int deleteEbookCateGoryById(Long id);


}
