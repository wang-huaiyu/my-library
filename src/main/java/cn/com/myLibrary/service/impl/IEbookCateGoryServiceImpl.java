package cn.com.myLibrary.service.impl;

import cn.com.myLibrary.mapper.EbookCateGoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  17:28
 * @Version 1.0
 * @Param $
 * @return $
 */
@Service
public class IEbookCateGoryServiceImpl implements IEbookCateGoryService {
    @Autowired
    private EbookCateGoryMapper ebookCateGoryMapper;

    /**
     * 查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public EbookCateGory selectEbookCateGoryById(Long id) {
        return ebookCateGoryMapper.selectEbookCateGoryById(id);
    }

    /**
     * 查询列表
     *
     * @param ebookCateGory
     * @return 集合
     */
    @Override
    public List<EbookCateGory> selectEbookCateGoryList(EbookCateGory ebookCateGory) {
        List<EbookCateGory> ebookCateGories = ebookCateGoryMapper.selectEbookCateGoryList(ebookCateGory);
        return ebookCateGories;
        //return ebookCateGoryMapper.selectEbookCateGoryList(ebookCateGory);
    }

    /**
     * 新增
     *
     * @return 结果
     * @parame ebookCateGory
     */
    @Override
    @Transactional
    public int insertEbookCateGory(EbookCateGory ebookCateGory) {
        return ebookCateGoryMapper.insertEbookCateGory(ebookCateGory);
    }

    /**
     * 修改
     *
     * @param ebookCateGory
     * @return 结果
     */
    @Override
    @Transactional
    public int updateEbookCateGory(EbookCateGory ebookCateGory) {
        return ebookCateGoryMapper.updateEbookCateGory(ebookCateGory);
    }

    /**
     * 批量删除
     *
     * @param ids 需要删除的主键集合
     * @return 结果
     */
    @Override
    public int deleteEbookCateGoryByIds(Long[] ids) {
        return ebookCateGoryMapper.deleteEbookCateGoryByIds(ids);
    }

    /**
     * 删除信息
     *
     * @param id 主键
     * @return 结果
     */
    @Override
    public int deleteEbookCateGoryById(Long id) {
        return ebookCateGoryMapper.deleteEbookCateGoryById(id);
    }
}
