package cn.com.myLibrary.service.impl;

import cn.com.myLibrary.mapper.EbookEntryMapper;
import cn.com.myLibrary.pojo.EbookEntry;
import cn.com.myLibrary.service.IEbookEntryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  15:22
 * @Version 1.0
 * @Param $
 * @return $
 */
@Service
public class EbookEntryServiceImpl implements IEbookEntryService {
    @Autowired
    private EbookEntryMapper ebookEntryMapper;

    /**
     * 查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public EbookEntry selectEbookEntryById(Long id) {
        return ebookEntryMapper.selectEbookEntryById(id);
    }

    /**
     * 查询列表
     *
     * @param ebookEntry
     * @return
     */
    @Override
    public PageInfo<EbookEntry> selectEbookEntryList(EbookEntry ebookEntry, Integer currentPageNo, Integer pageSize) {
        System.out.println("分页查询");
        PageInfo<EbookEntry> pageInfo = null;
        try {
            PageHelper.startPage(currentPageNo, pageSize);
            List<EbookEntry> ebookEntries = ebookEntryMapper.selectEbookEntryList(ebookEntry);
            //System.out.println("ebookEntries = " + ebookEntries);
            pageInfo = new PageInfo<EbookEntry>(ebookEntries);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageInfo;
    }

    /**
     * 新增
     *
     * @param ebookEntry
     * @return 结果
     */
    @Override
    @Transactional
    public int insertEbookEntry(EbookEntry ebookEntry) {
        int existsFlag = ebookEntryMapper.selectEbookEntryByTitleAndCateGroyId(ebookEntry.getCategoryId(), ebookEntry.getTitle());
        if (existsFlag >= 1) {
            return -1;
        } else {
            return ebookEntryMapper.insertEbookEntry(ebookEntry);
        }
    }

    /**
     * 修改
     *
     * @param ebookEntry
     * @return 结果
     */
    @Override
    @Transactional
    public int updateEbookEntry(EbookEntry ebookEntry) {
        int updateEbookEntryFlag = ebookEntryMapper.updateEbookEntry(ebookEntry);
        if (updateEbookEntryFlag >= 1) {
            return updateEbookEntryFlag;
        } else {
            return 0;
        }
    }

    /**
     * 批量删除
     *
     * @param ids 需要删除的主键
     * @return 结果
     */
    @Override
    public int deleteEbookEntryByIds(Long[] ids) {
        return ebookEntryMapper.deleteEbookEntryByIds(ids);
    }

    /**
     * 删除信息
     *
     * @param id 主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteEbookEntryById(Long id) {
        return ebookEntryMapper.deleteEbookEntryById(id);
    }

    /**
     * 查询数量
     *
     * @param ebookEntry
     */
    @Override
    @Transactional
    public int selectEbookEntryCount(EbookEntry ebookEntry) {
        return ebookEntryMapper.selectEbookEntryCount(ebookEntry);
    }
}
