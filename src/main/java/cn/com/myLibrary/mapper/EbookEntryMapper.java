package cn.com.myLibrary.mapper;

import cn.com.myLibrary.pojo.EbookEntry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  15:17
 * @Version 1.0
 * @Param $
 * @return $
 */
@Mapper
public interface EbookEntryMapper {
    /**
     * 查询
     *
     * @param id 主键
     * @return
     */
    public EbookEntry selectEbookEntryById(Long id);

    /**
     * @param categoryId
     * @param title
     * @return
     */
    public int selectEbookEntryByTitleAndCateGroyId(@Param("categoryId") Long categoryId, @Param("title") String title);

    /**
     * 查询列表
     *
     * @param ebookEntry
     * @return 集合
     */
    public List<EbookEntry> selectEbookEntryList(EbookEntry ebookEntry);

    /**
     * 查询数量
     */
    public int selectEbookEntryCount(EbookEntry ebookEntry);

    /**
     * 新增
     *
     * @param ebookEntry
     * @return 结果
     */
    public int insertEbookEntry(EbookEntry ebookEntry);

    /**
     * 修改
     *
     * @param ebookEntry
     * @return 结果
     */
    public int updateEbookEntry(EbookEntry ebookEntry);

    /**
     * 删除
     *
     * @param id 主键
     * @return 结果
     */
    public int deleteEbookEntryById(Long id);

    /**
     * 批量删除
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEbookEntryByIds(Long[] ids);
}
