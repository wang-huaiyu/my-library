package cn.com.myLibrary.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  14:39
 * @Version 1.0
 * @Param $
 * @return $
 */
@Data
public class EbookCateGory implements Serializable {
    private Long id;     // 分类id
    private String name; // 分类名称
}
