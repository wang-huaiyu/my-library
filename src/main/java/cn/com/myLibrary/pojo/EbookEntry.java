package cn.com.myLibrary.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author WangHuaiyu
 * Created by 529044029@qq.com
 * Date on 2024/4/26  14:40
 * @Version 1.0
 * @Param $
 * @return $
 */
@Data
public class EbookEntry implements Serializable {
    private Long id;
    @NotNull(message = "分类id不能为空")
    private Long categoryId;
    @NotNull(message = "书名不能为空")
    @NotEmpty(message = "书名不能为空字符串")
    private String title;
    private String summary;
    @NotNull(message = "上传者不能为空")
    @NotEmpty(message = "上传者不能为空字符串")
    private String uploadUser;
    @Past(message = "时间必须是一个过去时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createDate;
}
