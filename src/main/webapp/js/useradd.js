﻿var title = null;
var summary = null;
var uploadUser = null;
var createDate = null;
var categoryId = null;
var addBtn = null;
var backBtn = null;

$(function () {
    initCategory();
    title = $("#title");
    summary = $("#summary");
    uploadUser = $("#uploadUser");
    createDate = $("#createDate");
    categoryId = $("#categoryId");

    addBtn = $("#add");
    backBtn = $("#back");
    //初始化的时候，要把所有的提示信息变为：* 以提示必填项，更灵活，不要写在页面上
    title.next().html("*");
    summary.next().html("*");
    uploadUser.next().html("*");
    createDate.next().html("*");
    categoryId.next().html("*")

    var collection = {
        title: $("#title"),
        summary: $("#summary"),
        uploadUser: $("#uploadUser"),
        createDate: $("#createDate"),
        categoryId: $("#categoryId")
    };

    /*
     * 验证
     * 失焦\获焦
     * jquery的方法传递
     */
    title.bind("focus", function () {
        validateTip(title.next(), {"color": "#666666"}, "* 用户名长度必须是大于1小于10的字符", false);
    }).bind("blur", function () {
        if (title.val() != null && title.val().length > 1
            && title.val().length < 10) {
            validateTip(title.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(title.next(), {"color": "red"}, imgNo + " 用户名输入的不符合规范，请重新输入", false);
        }

    });

    summary.bind("focus", function () {
        validateTip(summary.next(), {"color": "#666666"}, "* 图书摘要长度必须是大于1小于80", false);
    }).bind("blur", function () {
        if (summary.val() != null && summary.val().length > 1
            && summary.val().length < 80) {
            validateTip(summary.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(summary.next(), {"color": "red"}, imgNo + " 图书摘要输入不符合规范，请重新输入", false);
        }
    });

    createDate.bind("focus", function () {
        validateTip(createDate.next(), {"color": "#666666"}, "* 点击输入框，选择日期", false);
    }).bind("blur", function () {
        if (createDate.val() != null && createDate.val() != "") {
            validateTip(createDate.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(createDate.next(), {"color": "red"}, imgNo + " 选择的日期不正确,请重新输入", false);
        }
    });

    uploadUser.on("focus", function () {
        validateTip(uploadUser.next(), {"color": "#666666"}, "* 上传人长度必须是大于2小于40的字符", false);
    }).on("blur", function () {
        if (uploadUser.val() != null && uploadUser.val().length >= 2
            && uploadUser.val().length < 40) {
            validateTip(uploadUser.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(uploadUser.next(), {"color": "red"}, imgNo + " 图书摘要输入的不符合规范，请重新输入", false);
        }
    });

    let categoryInfo = $("#categoryInfo"); // 获取用于显示错误信息的元素
    categoryId.bind("focus", function () {
        validateTip(categoryInfo, {"color": "#666666"}, "* 请选择图书类别", false);
    }).bind("blur", function () {
        if (categoryId.val() !== "" && parseInt(categoryId.val(), 10) > 0) {
            // 确保值是数字且大于0
            validateTip(categoryInfo, {"color": "green"}, imgYes, true);
        } else {
            validateTip(categoryInfo, {"color": "red"}, imgNo + " 请选择图书类别", false);
        }
    });

    addBtn.bind("click", function () {
        if (title.attr("validateStatus") != "true") {
            title.blur();
        } else if (summary.attr("validateStatus") != "true") {
            summary.blur();
        } else if (uploadUser.attr("validateStatus") != "true") {
            uploadUser.blur();
        } else if (createDate.attr("validateStatus") != "true") {
            createDate.blur();
        } else {
            if (confirm("是否确认提交数据")) {
                // 发起异步POST请求
                $.ajax({
                    type: 'POST',
                    url: $("#bookForm").attr('action'), // 获取表单的 action 属性值
                    contentType: 'application/json',
                    data: JSON.stringify({ // 将数据对象转换为 JSON 字符串
                        title: $("#title").val(),
                        summary: $("#summary").val(),
                        uploadUser: $("#uploadUser").val(),
                        createDate: $("#createDate").val(),
                        categoryId: $("#categoryId").val(),
                    }),
                    success: function (response) {
                        // 更新页面内容或显示提示信息
                        if (response.code == 500) {
                            toastr.warning("图书【" + $("#title").val() + "】," + "添加失败");
                            for (let key in response.data) {
                                if (response.data.hasOwnProperty(key)) {
                                    // 使用 key 和 response.data[key] 来处理每个属性
                                    alert("key:" + key + "value" + response.data[key]);
                                    // 检查collection中是否有与key匹配的属性
                                    if (collection.hasOwnProperty(key)) {
                                        validateTip(collection[key].next(), {"color": "red"}, imgNo + response.data[key], false);
                                    }
                                }
                            }

                        }
                        if (response.code == 200) {
                            toastr.success("图书【" + $("#title").val() + "】," + response.msg);
                            setTimeout(function () {
                                window.location.href = contextPath + '/book/list.do';
                            }, 600);
                        }
                    },
                    error: function (xhr, status, error) {
                        // 请求失败时的回调函数
                        toastr.warning("请求失败: ", error);
                    }
                });
            }
        }
    });

    backBtn.on("click", function () {
        if (referer != undefined
            && null != referer
            && "" != referer
            && "null" != referer
            && referer.length > 4) {
            window.location.href = referer;
        } else {
            history.back(-1);
        }
    });
});


function initCategory() {
    $.getJSON("/bookCateGory/list.do", {}, function (result) {
        console.log(result);
        $("select[name='categoryId']").empty();// 清空列表
        $("select[name='categoryId']").append(
            " <option value='' selected>--请选择--</option>"
        )
        $(result).each(function (i, o) {
            $("select[name='categoryId']").append(
                "<option value='" + o.id + "'>" + o.name + "</option>"
            )
        })
    });
}
function changeImg(value,t){
    var elementById = document.getElementById(value);
    previewImage(elementById,t);
}

function previewImage(obj,file){
    var reader = new FileReader();
    reader.onload = function(evt){
        obj.src = evt.target.result;
    };
    reader.readAsDataURL(file.files[0]);
}

function updateImg(btnId,imgId){
    if(window.confirm("是否修改图片")){
        $(imgId).click();
        $('.progress').css('display', "none");
        $('.progress > div').css('width', 0);
    }else {
        return false;
    }
    return false;
}

function unloadImg(uploadImg,fileId,imgId,progress,type,text){
    $(function(){
        var b = confirm("是否保存该图片");
        if(b==true){
            // 获取要上传的文件
            //fileId = $("#fileName");
            var photoFile =$(fileId)[0].files[0]
            if(photoFile==undefined){
                alert("您还未选中文件")
                return;
            }
            // 将文件装入FormData对象
            var formData =new FormData();
            formData.append("headPhoto",photoFile);
            // ajax向后台发送文件
            $.ajax({
                type:"post",
                data:formData,
                url:contextPath + "/file/upload.do",
                processData:false,
                contentType:false,
                success:function(result){
                    // 接收后台响应的信息
                    alert(result.message)
                    // 图片回显
                    $(imgId).attr("src","http://localhost:8080" + "/upload/"+result.newFileName);
                    $(text).val(result.newFileName);
                },
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    //使用XMLHttpRequest.upload监听上传过程，注册progress事件，打印回调函数中的event事件
                    xhr.upload.addEventListener('progress', function (e) {
                        //loaded代表上传了多少
                        //total总数为代表多少
                        var progressRate = (e.loaded / e.total) * 100 + '%';
                        //通过设置进度条的宽度达到效果
                        $(progress).css('display',"");
                        $(type).css('width', progressRate);

                    })
                    return xhr;
                }
            })
        }else {

        }
    })
}

