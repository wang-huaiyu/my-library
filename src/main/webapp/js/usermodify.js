﻿var title = null;
var summary = null;
var uploadUser = null;
var createDate = null;
var categoryId = null;
var saveBtn = null;
var backBtn = null;

$(function () {
    initCategory();
    title = $("#title");
    summary = $("#summary");
    uploadUser = $("#uploadUser");
    createDate = $("#createDate");
    categoryId = $("#categoryId");
    saveBtn = $("#save");
    backBtn = $("#back");

    title.next().html("*");
    summary.next().html("*");
    uploadUser.next().html("*");
    createDate.next().html("*");
    categoryId.next().html("*");


    title.on("focus", function () {
        validateTip(title.next(), {"color": "#666666"}, "* 图书名称长度必须是大于1小于40的字符", false);
    }).on("blur", function () {
        if (title.val() != null && title.val().length > 1
            && title.val().length < 40) {
            validateTip(title.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(title.next(), {"color": "red"}, imgNo + " 图书名称输入的不符合规范，请重新输入", false);
        }

    });

    summary.on("focus", function () {
        validateTip(summary.next(), {"color": "#666666"}, "* 图书摘要长度必须是大于1小于80的字符", false);
    }).on("blur", function () {
        if (summary.val() != null && summary.val().length > 1
            && summary.val().length < 80) {
            validateTip(summary.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(summary.next(), {"color": "red"}, imgNo + " 图书摘要输入的不符合规范，请重新输入", false);
        }
    });


    uploadUser.on("focus", function () {
        validateTip(uploadUser.next(), {"color": "#666666"}, "* 上传人长度必须是大于2小于40的字符", false);
    }).on("blur", function () {
        if (uploadUser.val() != null && uploadUser.val().length >= 2
            && uploadUser.val().length < 40) {
            validateTip(uploadUser.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(uploadUser.next(), {"color": "red"}, imgNo + " 图书摘要输入的不符合规范，请重新输入", false);
        }
    });

    createDate.on("focus", function () {
        validateTip(createDate.next(), {"color": "#666666"}, "* 点击输入框，选择日期", false);
    }).on("blur", function () {
        if (createDate.val() != null && createDate.val() != "") {
            validateTip(createDate.next(), {"color": "green"}, imgYes, true);
        } else {
            validateTip(createDate.next(), {"color": "red"}, imgNo + " 选择的日期不正确,请重新输入", false);
        }
    });

    saveBtn.bind("click", function () {
        if (title.attr("validateStatus") != "true") {
            title.blur();
        } else if (summary.attr("validateStatus") != "true") {
            summary.blur();
        } else if (uploadUser.attr("validateStatus") != "true") {
            uploadUser.blur();
        } else if (createDate.attr("validateStatus") != "true") {
            createDate.blur();
        } else {
            if (confirm("是否确认保存数据")) {
                $.ajax({
                    type: 'PUT',
                    url: $("#bookForm").attr('action'), // 获取表单的 action 属性值
                    contentType: 'application/json',
                    data: JSON.stringify({ // 将数据对象转换为 JSON 字符串
                        id:$("#bookid").val(),
                        title: $("#title").val(),
                        summary: $("#summary").val(),
                        uploadUser: $("#uploadUser").val(),
                        createDate: $("#createDate").val(),
                        categoryId: $("#categoryId").val(),
                    }),
                    success: function (response) {
                        // 更新页面内容或显示提示信息
                        if (response.code == 500) {
                            toastr.warning(response.msg);
                        }
                        if (response.code == 200) {
                            toastr.success(response.msg);
                            setTimeout(function () {
                                window.location.href = contextPath + '/book/list.do';
                            }, 600);
                        }
                    },
                    error: function (xhr, status, error) {
                        // 请求失败时的回调函数
                        console.error("请求失败: ", error);
                    }
                });
            }
        }


    });

    backBtn.on("click", function () {
        if (referer != undefined
            && null != referer
            && "" != referer
            && "null" != referer
            && referer.length > 4) {
            window.location.href = referer;
        } else {
            history.back(-1);
        }
    });
});

function initCategory() {
    $.getJSON("/bookCateGory/list.do", {}, function (result) {
        console.log(result);

        $("select[name='categoryId']").empty();// 清空列表
        let valId = $("#categoryIds").val();
        $(result).each(function (i, o) {
            if (o.id == valId) {
                $("select[name='categoryId']").append(
                    "<option value='" + o.id + "' selected>" + o.name + "</option>"
                )
            } else {
                $("select[name='categoryId']").append(
                    "<option value='" + o.id + "' >" + o.name + "</option>"
                )
            }
        })
    });
}
