﻿var userObj;

//用户管理页面上点击删除按钮弹出删除框(userlist.jsp)
function deleteUser(obj) {
    $.ajax({
        type: "delete",
        url: contextPath + "/book/delete.do?id=" + obj.attr("bookid"),
        dataType: "json",
        success: function (response) {
            if (response.code == 200) {
                //删除成功：移除删除行
                cancleBtn();
                setTimeout(function () {
                    toastr.success("图书【" + obj.attr("bookname")  +"】,"  +  response.msg);
                }, 800); // 2秒后执行
                obj.parents("tr").remove();
            }
            if (response.code == 500) {
                //删除失败
                toastr.error("图书【" + obj.attr("bookname")  +"】," + response.msg);
            }
        },
        error: function (data) {
            toastr.error("对不起，删除失败");
        }
    });
}

function openYesOrNoDLG() {
    $('.zhezhao').css('display', 'block');
    $('#removeUse').fadeIn();
}

function cancleBtn() {
    $('.zhezhao').css('display', 'none');
    $('#removeUse').fadeOut();
}

function changeDLGContent(contentStr) {
    var p = $(".removeMain").find("p");
    p.html(contentStr);
}

$(function () {
    /**
     *
     */
    $(".modifyUser").on("click", function () {
        var obj = $(this);
        window.location.href = contextPath + "/book/query.do?bookid=" + obj.attr("bookid");
    });


    $('#no').click(function () {
        cancleBtn();
    });

    $('#yes').click(function () {
        deleteUser(userObj);
    });

    $(".deleteUser").on("click", function () {
        userObj = $(this);
        changeDLGContent("你确定要删除图书【" + userObj.attr("bookname") + "】吗？");
        openYesOrNoDLG();
    });
});

