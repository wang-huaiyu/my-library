<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>图书管理系统</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/public.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/toastr.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/layui.css" charset="utf-8"
          media="all"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/time.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/toastr.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/calendar/WdatePicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/useradd.js"></script>
    <script type="text/javascript">
        toastr.options.positionClass = 'toast-top-center';
    </script>

    <script type="text/javascript">
        var contextPath = '<%= request.getContextPath() %>';
    </script>
</head>
<div class="right">
    <div class="location">
        <strong>你现在所在的位置是:</strong>
        <span>图书管理页面 >> 图书添加页面</span>
    </div>
    <div class="providerAdd">
        <form id="bookForm" name="bookForm" method="post" action="${pageContext.request.contextPath }/book/save.do">
            <div>
                <label for="title">图书名称：</label>
                <input type="text" name="title" id="title" value="">
                <font color="red"></font>
            </div>
            <div>
                <label for="summary">图书摘要：</label>
                <input type="text" name="summary" id="summary" value="">
                <font color="red"></font>
            </div>
            <div>
                <label for="uploadUser">上传人：</label>
                <input type="text" name="uploadUser" id="uploadUser" value="">
                <font color="red"></font>
            </div>

            <div>
                <label for="createDate">上传日期：</label>
                <input type="date" Class="Wdate" id="createDate" name="createDate" readonly="readonly"
                       onclick="WdatePicker();">
                <font color="red"></font>
            </div>

            <%--<div>
                <label for="categoryId" >图书类别：</label>
                <select name="categoryId" id="categoryId"></select>
                <div id="categoryInfo" class="error-message"></div>
            </div>--%>

            <div style="display: flex; align-items: center;">
                <label for="categoryId">图书类别：</label>
                <select name="categoryId" id="categoryId"></select>
                <div id="categoryInfo" class="error-message"></div>
            </div>

            <div style="display: flex; align-items: center;">
                <label for="img">图书图片：</label>
                <img src="http://127.0.0.1:8080/upload/dbf8d5fa-39d5-4b3a-9b76-c8be24c2fc09.jpg" id="img"
                     style="width: 450px;height: 260px;background-size: 100% 100%">
                <input id="fileNamemain" name="fileNamemain" hidden type="hidden" text="" value=""  style="width:446px" readonly unselectable="on">
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block" style="padding-left: 250px">
                    <input type="file" style="width: 300px;display: none" id="fileName" text="xxxxx" accept=".jpg,.png"
                           value="" autocomplete="on" class="layui-input" onchange="changeImg('img',this)">
                    <a href="javascript:void(0);" id="btnfileName" type="button" autocomplete="on"
                       onclick="updateImg('#btnfileName','#fileName',this)">
                        <span class="iconfont icon-bianji">上传图片</span>
                    </a>
                    <a id="upload" href="javascript:void(0);"
                       onclick="unloadImg('#upload','#fileName','#img','#progressMain','#progressMainType','#fileNamemain')">
                        <span class="iconfont icon-166991">图片保存</span>
                    </a>
                </div>
            </div>
            <div class="layui-form-item">
                <div id="progressMain" class="progress" style="display: none">
                    <div id="progressMainType"></div>
                </div>
            </div>


            <div class="providerAddBtn">
                <input type="button" name="add" id="add" value="保存">
                <input type="button" id="back" name="back" value="返回">
            </div>
        </form>
    </div>
</div>
</html>

，
