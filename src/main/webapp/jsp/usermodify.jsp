<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>图书管理系统</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/public.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/toastr.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/time.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/toastr.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/calendar/WdatePicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/usermodify.js"></script>
    <script type="text/javascript">
        toastr.options.positionClass = 'toast-top-center';
    </script>
    <script type="text/javascript">
        var contextPath = '<%= request.getContextPath() %>';
    </script>
</head>
<div class="right">
    <div class="location">
        <strong>你现在所在的位置是:</strong>
        <span>图书管理页面 >> 图书修改页面</span>
    </div>
    <div class="providerAdd">
        <form id="bookForm" name="bookForm" method="post" action="${pageContext.request.contextPath }/book/update.do">
            <input hidden id="bookid" name="bookid" value="${book.id}">
            <div>

                <label for="title">图书名称：</label>
                <input type="text" name="title" id="title" value="${book.title}">
                <font color="red"></font>
            </div>
            <div>
                <label for="summary">图书摘要：</label>
                <input type="text" name="summary" id="summary" value="${book.summary}">
                <font color="red"></font>
            </div>
            <div>
                <label for="uploadUser">上传人：</label>
                <input type="text" name="uploadUser" id="uploadUser" value="${book.uploadUser}">
                <font color="red"></font>
            </div>

            <div>

                <label for="createDate">上传日期：</label>
               <%-- <input type="date" Class="Wdate" id="createDate" name="createDate"
                       value="formattedDate">--%>

                <fmt:formatDate var="formattedCreateDate" value="${book.createDate}" pattern="yyyy-MM-dd" />
                <input type="date" class="Wdate" id="createDate" name="createDate" readonly="readonly"
                     onclick="WdatePicker()"  value="${formattedCreateDate}" />
                <font color="red"></font>
            </div>

            <div>
                <label for="categoryId">图书类别：</label>
                <input hidden id="categoryIds" alue="${book.categoryId}"/>
                <select name="categoryId" id="categoryId"></select>
            </div>

            <div class="providerAddBtn">
                <input type="button" name="save" id="save" value="保存">
                <input type="button" id="back" name="back" value="返回">
            </div>
        </form>
    </div>
</div>
</html>
