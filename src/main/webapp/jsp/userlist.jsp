<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>图书管理系统</title>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css" />
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/public.css" />
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/css/toastr.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/time.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/toastr.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/calendar/WdatePicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/userlist.js"></script>
	<script type="text/javascript">
		toastr.options.positionClass = 'toast-top-center';
	</script>

	<script type="text/javascript">
		var contextPath = '<%= request.getContextPath() %>';
	</script>
</head>
        <div class="right">
            <div class="location">
                <strong>你现在所在的位置是:</strong>
                <span>图书管理页面</span>
            </div>
            <div class="search">
           		<form method="get" action="${pageContext.request.contextPath }/book/list.do">
					 <span>图书名称：</span>
					 <input name="queryBookName" class="input-text"	type="text" value="${queryBookName}">

					 <span>图书分类：</span>
					 <select name="category">
						 <c:if test="${categoryList != null }">
							 <option value="0">--请选择--</option>
							 <c:forEach var="cateGroy" items="${categoryList}">
								 <option <c:if test="${cateGroy.id == category }">selected="selected"</c:if>
										 value="${cateGroy.id}">${cateGroy.name}</option>
							 </c:forEach>
						 </c:if>
	        		</select>

					 <input type="hidden" name="pageIndex" value="1"/>
					 <input	value="查 询" type="submit" id="searchbutton">
					 <a href="${pageContext.request.contextPath}/jsp/useradd.jsp" >添加图书</a>
				</form>
            </div>
            <!--用户-->
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tr class="firstTr">
                    <th width="10%">图书编号</th>
                    <th width="20%">图书名称</th>
                    <th width="10%">图书摘要</th>
                    <th width="10%">上传人</th>
                    <th width="10%">上传时间</th>
                    <th width="30%">操作</th>
                </tr>
                   <c:forEach var="ebook" items="${ebookEntryList }" varStatus="status">
					<tr>
						<td>
						<span>${ebook.id}</span>
						</td>
						<td>
						<span>${ebook.title}</span>
						</td>
						<td>
							<span>
									${ebook.summary}
							</span>
						</td>
						<td>
						<span>${ebook.uploadUser}</span>
						</td>
						<td>
						<span><fmt:formatDate value="${ebook.createDate}" pattern="yyyy-MM-dd" /></span>
						</td>
						<td>
						<span><a class="viewUser" href="javascript:;" bookid=${ebook.id } bookname=${ebook.title }><img src="${pageContext.request.contextPath }/images/read.png" alt="查看" title="查看"/></a></span>
						<span><a class="modifyUser" href="javascript:;" bookid=${ebook.id } bookname=${ebook.title }><img src="${pageContext.request.contextPath }/images/xiugai.png" alt="修改" title="修改"/></a></span>
						<span><a class="deleteUser" href="javascript:;" bookid=${ebook.id } bookname=${ebook.title }><img src="${pageContext.request.contextPath }/images/schu.png" alt="删除" title="删除"/></a></span>
						</td>
					</tr>
				</c:forEach>
			</table>
			<input type="hidden" id="totalPageCount" value="${totalPageCount}"/>
		  	<c:import url="rollpage.jsp">
	          	<c:param name="totalCount" value="${totalCount}"/>
	          	<c:param name="currentPageNo" value="${currentPageNo}"/>
	          	<c:param name="totalPageCount" value="${totalPageCount}"/>
          	</c:import>
        </div>
    </section>

<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeUse">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该用户吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>
</html>
